CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration	
 * Maintainers
 
 INTRODUCTION
------------
Simple Meta for Content Types extends the Simple Meta module allowing you 
to set meta tag patterns for content types in addition to meta tag patterns 
per path.
   
   REQUIREMENTS
------------
This module requires the following modules:
 * Simple Meta (https://www.drupal.org/project/simplemeta)
 
 INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
   CONFIGURATION
-------------
 * Configure simple meta per content type by visiting the edit page for each
content type and looking in the "Simple Meta patterns" section
 
 MAINTAINERS
-----------
Current maintainers:
 * Drew Michael (drubage) - https://drupal.org/user/212502

This project has been sponsored by:
 * Fruition.net
   Fruition is a full service digital agency providing Internet marketing, 
   design, development, and digital security services for businesses and 
   governments.
   Visit https://fruition.net for more information.
